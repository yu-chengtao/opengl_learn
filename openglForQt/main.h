#ifndef MAIN_H
#define MAIN_H
#include <QApplication>
#include <QDebug>
#include <QFileInfo>
#include <QDir>
#include <iostream>
#include <cmath>
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "openglTool/camera.h"
#include "openglTool/model.h"

// 使用pro文件中的自定义  MY_CUSTOM_PATH 宏
QString currentDir = MY_CUSTOM_PATH;

float mixValue = 0.2f; //透明度

// settings
const unsigned int SCR_WIDTH = 800;
const unsigned int SCR_HEIGHT = 600;

Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
float lastX = SCR_WIDTH / 2.0f;
float lastY = SCR_HEIGHT / 2.0f;
bool firstMouse = true;

float deltaTime = 0.0f; // 当前帧与上一帧的时间差
float lastFrame = 0.0f; // 上一帧的时间

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow *window);
void transformation(Shader &ourShader);
QString getFilePath(QString path);
unsigned int loadTexture(const char *path);
unsigned int loadCubemap(std::vector<std::string> faces);

#endif // MAIN_H
