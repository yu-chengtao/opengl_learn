#version 330 core
layout (location = 0) in vec3 aPos;
layout (location = 1) in vec3 aNormal;
layout (location = 2) in vec2 aTexCoords;

out vec3 FragPos;//片段的位置
out vec3 Normal;//法向量
out vec2 TexCoords;//材质坐标

uniform mat4 model;
uniform mat4 view;
uniform mat4 projection;

void main()
{
    FragPos = vec3(model * vec4(aPos, 1.0)); //片段的位置
    //法线矩阵被定义为「模型矩阵左上角3x3部分的逆矩阵的转置矩阵」 用于处理不等比缩放
    Normal = mat3(transpose(inverse(model))) *aNormal;
    gl_Position = projection * view * model * vec4(aPos, 1.0);
    TexCoords = aTexCoords;
}
