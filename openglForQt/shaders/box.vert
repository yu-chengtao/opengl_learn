#version 330 core
layout (location = 0) in vec3 aPos;

uniform mat4 model1;
uniform mat4 projection1;
uniform mat4 view1;

void main()
{
    gl_Position = projection1 * view1 * model1 * vec4(aPos, 1.0);
}
