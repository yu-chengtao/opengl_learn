#version 330 core
out vec4 FragColor;//片段颜色

struct DirLight {
    vec3 direction;//定向光

    vec3 ambient;//环境光
    vec3 diffuse;//漫反射
    vec3 specular;//镜面光
};

//点光源
struct PointLight {
    vec3 position; //光源位置

    float constant; //常数项保持为1.0，它的主要作用是保证分母永远不会比1小
    float linear;   //距离值相乘，线性的方式减少强度
    float quadratic;//二次项会与距离的平方相乘，让光源以二次递减的方式减少强度

    vec3 ambient;//环境光
    vec3 diffuse;//漫反射
    vec3 specular;//镜面光
};

struct SpotLight {
    vec3 position;
    vec3 direction;
    float cutOff;//内圆锥
    float outerCutOff;//外圆锥

    float constant;//常量
    float linear;//线性
    float quadratic;//二次

    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

struct Material {
    //采样器内置类型
    //木制箱子 漫反射
    sampler2D diffuse;
    //铁框 镜面光照
    sampler2D specular;
    //光条
    sampler2D matrix;
    //高光的反光度(Shininess)
    //一个物体的反光度越高，反射光的能力越强，散射得越少，高光点就会越小。
    float shininess;
};

const int NR_POINT_LIGHTS  = 4; //常量

in vec2 TexCoords;//材质坐标
in vec3 Normal; //法线
in vec3 FragPos;

uniform vec3 viewPos;//观察者
uniform DirLight dirLight;
uniform SpotLight spotLight;
uniform PointLight pointLights[NR_POINT_LIGHTS];
uniform Material material;

//函数
vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir);//定向光
vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir);//点光源
vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir);

void main()
{
    // 属性
    vec3 norm = normalize(Normal);
    vec3 viewDir = normalize(viewPos - FragPos);

    // 第一阶段：定向光照
    vec3 result = CalcDirLight(dirLight, norm, viewDir);
//     第二阶段：点光源
    for(int i = 0; i < NR_POINT_LIGHTS; i++)
        result += CalcPointLight(pointLights[i], norm, FragPos, viewDir);
    // 第三阶段：聚光
    result += CalcSpotLight(spotLight, norm, FragPos, viewDir);

    FragColor = vec4(result, 1.0);
}

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 viewDir)
{
    vec3 lightDir = normalize(-light.direction);
    // 漫反射着色
    float diff = max(dot(normal, lightDir), 0.0);
    // 镜面光着色
    //reflect 函数用于计算一个向量关于另一个向量的反射向量
    //第一个向量是从光源指向片段位置的向量
    //但是lightDir当前正好相反，是从片段指向光源（由先前我们计算lightDir向量时，减法的顺序决定）
    //第二个参数要求是一个法向量
    vec3 reflectDir = reflect(-lightDir, normal);
    //叠加颜色（光强，镜面分量，纹理颜色）
    //计算镜面分量，点乘(反光强度)并确保不为负值，然后取shininess次幂
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    // 合并结果
    vec3 ambient  = light.ambient  * vec3(texture(material.diffuse, TexCoords));
    vec3 diffuse  = light.diffuse  * diff * vec3(texture(material.diffuse, TexCoords));
    vec3 specular = light.specular * spec * vec3(texture(material.specular, TexCoords));
    return (ambient + diffuse + specular);
}

vec3 CalcPointLight(PointLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);
    // 漫反射着色
    float diff = max(dot(normal, lightDir), 0.0);
    // 镜面光着色
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    // 衰减
    float distance    = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance +
                 light.quadratic * (distance * distance));
    // 合并结果
    vec3 ambient  = light.ambient  * vec3(texture(material.diffuse, TexCoords));
    vec3 diffuse  = light.diffuse  * diff * vec3(texture(material.diffuse, TexCoords));
    vec3 specular = light.specular * spec * vec3(texture(material.specular, TexCoords));
    ambient  *= attenuation;
    diffuse  *= attenuation;
    specular *= attenuation;
    return (ambient + diffuse + specular);
}

vec3 CalcSpotLight(SpotLight light, vec3 normal, vec3 fragPos, vec3 viewDir)
{
    vec3 lightDir = normalize(light.position - fragPos);
    // diffuse shading
    float diff = max(dot(normal, lightDir), 0.0);
    // specular shading
    vec3 reflectDir = reflect(-lightDir, normal);
    float spec = pow(max(dot(viewDir, reflectDir), 0.0), material.shininess);
    // attenuation
    float distance = length(light.position - fragPos);
    float attenuation = 1.0 / (light.constant + light.linear * distance + light.quadratic * (distance * distance));
    // spotlight intensity
    float theta = dot(lightDir, normalize(-light.direction));
    float epsilon = light.cutOff - light.outerCutOff;
    float intensity = clamp((theta - light.outerCutOff) / epsilon, 0.0, 1.0);
    // combine results
    vec3 ambient = light.ambient * vec3(texture(material.diffuse, TexCoords));
    vec3 diffuse = light.diffuse * diff * vec3(texture(material.diffuse, TexCoords));
    vec3 specular = light.specular * spec * vec3(texture(material.specular, TexCoords));
    ambient *= attenuation * intensity;
    diffuse *= attenuation * intensity;
    specular *= attenuation * intensity;
    return (ambient + diffuse + specular);
}
