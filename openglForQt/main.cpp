﻿#include "main.h"

//光源
glm::vec3 lightPos(1.2f, 1.0f, 2.0f);

using namespace std;

int main(int argc, char *argv[])
{

    // glfw: initialize and configure
    // ------------------------------
    glfwInit();
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 3);
    glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_CORE_PROFILE);

    //接下来我们创建一个窗口对象
    GLFWwindow* window = glfwCreateWindow(SCR_WIDTH , SCR_HEIGHT , "LearnOpenGL", NULL, NULL);
    if (window == NULL)
    {
        std::cout << "Failed to create GLFW window" << std::endl;
        glfwTerminate();
        return -1;
    }
    glfwMakeContextCurrent(window);
    glfwSetFramebufferSizeCallback(window, framebuffer_size_callback);
    glfwSetCursorPosCallback(window, mouse_callback);
    glfwSetScrollCallback(window, scroll_callback);

    //隐藏光标
    glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);

    if (!gladLoadGLLoader((GLADloadproc)glfwGetProcAddress))
    {
        std::cout << "Failed to initialize GLAD" << std::endl;
        return -1;
    }

    //Z缓冲
    glEnable(GL_DEPTH_TEST);//开启深度测试

    //    Shader defaultShader(getFilePath("/shaders/default.vert").toUtf8().data(),
    //                         getFilePath("/shaders/default.frag").toUtf8().data());

    //    Shader boxShader(getFilePath("/shaders/box.vert").toUtf8().data(),
    //                     getFilePath("/shaders/box.frag").toUtf8().data());

    //    Shader modelShader(getFilePath("/shaders/model.vert").toUtf8().data(),
    //                       getFilePath("/shaders/model.frag").toUtf8().data(),
    //                       getFilePath("/shaders/model.geom").toUtf8().data());

    //    Shader skyShader(getFilePath("/shaders/sky.vert").toUtf8().data(),
    //                     getFilePath("/shaders/sky.frag").toUtf8().data());

    //    Shader geomShader(getFilePath("/shaders/geometry.vert").toUtf8().data(),
    //                      getFilePath("/shaders/geometry.frag").toUtf8().data(),
    //                      getFilePath("/shaders/geometry.geom").toUtf8().data());

    Shader instanceShader(getFilePath("/shaders/instance.vert").toUtf8().data(),
                          getFilePath("/shaders/instance.frag").toUtf8().data());

    //    //告诉stb_image.h在y轴上翻转加载的纹理（在加载模型之前）。
    //    //    stbi_set_flip_vertically_on_load(true);
    //    Model ourModel(getFilePath("/resource/nanosuit/nanosuit.obj").toUtf8().data());
    //    Model ourModel2(getFilePath("/resource/nanosuit/nanosuit.obj").toUtf8().data());

    Shader asteroidShader(getFilePath("/shaders/asteroids.vert").toUtf8().data(),
                          getFilePath("/shaders/asteroids.frag").toUtf8().data());
    Shader planetShader(getFilePath("/shaders/planet.vert").toUtf8().data(),
                        getFilePath("/shaders/planet.frag").toUtf8().data());

    Model rock(getFilePath("/resource/rock/rock.obj").toUtf8().data());
    Model planet(getFilePath("/resource/planet/planet.obj").toUtf8().data());

    // generate a large list of semi-random model transformation matrices
    // ------------------------------------------------------------------
    unsigned int amount = 1000;
    glm::mat4* modelMatrices;
    modelMatrices = new glm::mat4[amount];
    srand(static_cast<unsigned int>(glfwGetTime())); // initialize random seed
    float radius = 50.0;
    float offset = 2.5f;
    for (unsigned int i = 0; i < amount; i++)
    {
        glm::mat4 model = glm::mat4(1.0f);
        // 1. translation: displace along circle with 'radius' in range [-offset, offset]
        float angle = (float)i / (float)amount * 360.0f;
        float displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
        float x = sin(angle) * radius + displacement;
        displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
        float y = displacement * 0.4f; // keep height of asteroid field smaller compared to width of x and z
        displacement = (rand() % (int)(2 * offset * 100)) / 100.0f - offset;
        float z = cos(angle) * radius + displacement;
        model = glm::translate(model, glm::vec3(x, y, z));

        // 2. scale: Scale between 0.05 and 0.25f
        float scale = static_cast<float>((rand() % 20) / 100.0 + 0.05);
        model = glm::scale(model, glm::vec3(scale));

        // 3. rotation: add random rotation around a (semi)randomly picked rotation axis vector
        float rotAngle = static_cast<float>((rand() % 360));
        model = glm::rotate(model, rotAngle, glm::vec3(0.4f, 0.6f, 0.8f));

        // 4. now add to list of matrices
        modelMatrices[i] = model;
    }
    // 顶点缓冲对象
    unsigned int buffer;
    glGenBuffers(1, &buffer);
    glBindBuffer(GL_ARRAY_BUFFER, buffer);
    glBufferData(GL_ARRAY_BUFFER, amount * sizeof(glm::mat4), &modelMatrices[0], GL_STATIC_DRAW);

    for(unsigned int i = 0; i < rock.meshes.size(); i++)
    {
        unsigned int VAO = rock.meshes[i].VAO;
        glBindVertexArray(VAO);
        // 顶点属性
        GLsizei vec4Size = sizeof(glm::vec4);
        glEnableVertexAttribArray(3);
        glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)0);
        glEnableVertexAttribArray(4);
        glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(vec4Size));
        glEnableVertexAttribArray(5);
        glVertexAttribPointer(5, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(2 * vec4Size));
        glEnableVertexAttribArray(6);
        glVertexAttribPointer(6, 4, GL_FLOAT, GL_FALSE, 4 * vec4Size, (void*)(3 * vec4Size));

        glVertexAttribDivisor(3, 1);
        glVertexAttribDivisor(4, 1);
        glVertexAttribDivisor(5, 1);
        glVertexAttribDivisor(6, 1);

        glBindVertexArray(0);
    }

    //    float cubeVertices[] = {
    //        // positions       // texture Coords  //normal
    //        -0.5f, -0.5f, -0.5f,     0.0f, 0.0f,    0.0f,  0.0f, -1.0f,
    //        0.5f, -0.5f, -0.5f,     1.0f, 0.0f,     0.0f,  0.0f, -1.0f,
    //        0.5f,  0.5f, -0.5f,     1.0f, 1.0f,     0.0f,  0.0f, -1.0f,
    //        0.5f,  0.5f, -0.5f,     1.0f, 1.0f,     0.0f,  0.0f, -1.0f,
    //        -0.5f,  0.5f, -0.5f,     0.0f, 1.0f,    0.0f,  0.0f, -1.0f,
    //        -0.5f, -0.5f, -0.5f,     0.0f, 0.0f,    0.0f,  0.0f, -1.0f,

    //        -0.5f, -0.5f,  0.5f,     0.0f, 0.0f,    0.0f,  0.0f, 1.0f,
    //        0.5f, -0.5f,  0.5f,     1.0f, 0.0f,     0.0f,  0.0f, 1.0f,
    //        0.5f,  0.5f,  0.5f,     1.0f, 1.0f,     0.0f,  0.0f, 1.0f,
    //        0.5f,  0.5f,  0.5f,     1.0f, 1.0f,     0.0f,  0.0f, 1.0f,
    //        -0.5f,  0.5f,  0.5f,     0.0f, 1.0f,    0.0f,  0.0f, 1.0f,
    //        -0.5f, -0.5f,  0.5f,     0.0f, 0.0f,    0.0f,  0.0f, 1.0f,

    //        -0.5f,  0.5f,  0.5f,     1.0f, 0.0f,   -1.0f,  0.0f,  0.0f,
    //        -0.5f,  0.5f, -0.5f,     1.0f, 1.0f,   -1.0f,  0.0f,  0.0f,
    //        -0.5f, -0.5f, -0.5f,     0.0f, 1.0f,   -1.0f,  0.0f,  0.0f,
    //        -0.5f, -0.5f, -0.5f,     0.0f, 1.0f,   -1.0f,  0.0f,  0.0f,
    //        -0.5f, -0.5f,  0.5f,     0.0f, 0.0f,   -1.0f,  0.0f,  0.0f,
    //        -0.5f,  0.5f,  0.5f,     1.0f, 0.0f,   -1.0f,  0.0f,  0.0f,

    //        0.5f,  0.5f,  0.5f,     1.0f, 0.0f,     1.0f,  0.0f,  0.0f,
    //        0.5f,  0.5f, -0.5f,     1.0f, 1.0f,     1.0f,  0.0f,  0.0f,
    //        0.5f, -0.5f, -0.5f,     0.0f, 1.0f,     1.0f,  0.0f,  0.0f,
    //        0.5f, -0.5f, -0.5f,     0.0f, 1.0f,     1.0f,  0.0f,  0.0f,
    //        0.5f, -0.5f,  0.5f,     0.0f, 0.0f,     1.0f,  0.0f,  0.0f,
    //        0.5f,  0.5f,  0.5f,     1.0f, 0.0f,     1.0f,  0.0f,  0.0f,

    //        -0.5f, -0.5f, -0.5f,     0.0f, 1.0f,    0.0f, -1.0f,  0.0f,
    //        0.5f, -0.5f, -0.5f,     1.0f, 1.0f,     0.0f, -1.0f,  0.0f,
    //        0.5f, -0.5f,  0.5f,     1.0f, 0.0f,     0.0f, -1.0f,  0.0f,
    //        0.5f, -0.5f,  0.5f,     1.0f, 0.0f,     0.0f, -1.0f,  0.0f,
    //        -0.5f, -0.5f,  0.5f,     0.0f, 0.0f,    0.0f, -1.0f,  0.0f,
    //        -0.5f, -0.5f, -0.5f,     0.0f, 1.0f,    0.0f, -1.0f,  0.0f,

    //        -0.5f,  0.5f, -0.5f,     0.0f, 1.0f,    0.0f,  1.0f,  0.0f,
    //        0.5f,  0.5f, -0.5f,     1.0f, 1.0f,     0.0f,  1.0f,  0.0f,
    //        0.5f,  0.5f,  0.5f,     1.0f, 0.0f,     0.0f,  1.0f,  0.0f,
    //        0.5f,  0.5f,  0.5f,     1.0f, 0.0f,     0.0f,  1.0f,  0.0f,
    //        -0.5f,  0.5f,  0.5f,     0.0f, 0.0f,    0.0f,  1.0f,  0.0f,
    //        -0.5f,  0.5f, -0.5f,     0.0f, 1.0f,     0.0f,  1.0f,  0.0f
    //    };

    //    float skyboxVertices[] = {
    //        // positions
    //        -1.0f,  1.0f, -1.0f,
    //        -1.0f, -1.0f, -1.0f,
    //        1.0f, -1.0f, -1.0f,
    //        1.0f, -1.0f, -1.0f,
    //        1.0f,  1.0f, -1.0f,
    //        -1.0f,  1.0f, -1.0f,

    //        -1.0f, -1.0f,  1.0f,
    //        -1.0f, -1.0f, -1.0f,
    //        -1.0f,  1.0f, -1.0f,
    //        -1.0f,  1.0f, -1.0f,
    //        -1.0f,  1.0f,  1.0f,
    //        -1.0f, -1.0f,  1.0f,

    //        1.0f, -1.0f, -1.0f,
    //        1.0f, -1.0f,  1.0f,
    //        1.0f,  1.0f,  1.0f,
    //        1.0f,  1.0f,  1.0f,
    //        1.0f,  1.0f, -1.0f,
    //        1.0f, -1.0f, -1.0f,

    //        -1.0f, -1.0f,  1.0f,
    //        -1.0f,  1.0f,  1.0f,
    //        1.0f,  1.0f,  1.0f,
    //        1.0f,  1.0f,  1.0f,
    //        1.0f, -1.0f,  1.0f,
    //        -1.0f, -1.0f,  1.0f,

    //        -1.0f,  1.0f, -1.0f,
    //        1.0f,  1.0f, -1.0f,
    //        1.0f,  1.0f,  1.0f,
    //        1.0f,  1.0f,  1.0f,
    //        -1.0f,  1.0f,  1.0f,
    //        -1.0f,  1.0f, -1.0f,

    //        -1.0f, -1.0f, -1.0f,
    //        -1.0f, -1.0f,  1.0f,
    //        1.0f, -1.0f, -1.0f,
    //        1.0f, -1.0f, -1.0f,
    //        -1.0f, -1.0f,  1.0f,
    //        1.0f, -1.0f,  1.0f
    //    };

    //    // cube VAO
    //    unsigned int cubeVAO, cubeVBO;
    //    glGenVertexArrays(1, &cubeVAO);
    //    glGenBuffers(1, &cubeVBO);
    //    glBindVertexArray(cubeVAO);
    //    glBindBuffer(GL_ARRAY_BUFFER, cubeVBO);
    //    glBufferData(GL_ARRAY_BUFFER, sizeof(cubeVertices), &cubeVertices, GL_STATIC_DRAW);
    //    glEnableVertexAttribArray(0);
    //    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)0);
    //    glEnableVertexAttribArray(1);
    //    glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(3 * sizeof(float)));
    //    glEnableVertexAttribArray(2);
    //    glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 8 * sizeof(float), (void*)(5 * sizeof(float)));
    //    glBindVertexArray(0);
    //    // skybox VAO
    //    unsigned int skyboxVAO, skyboxVBO;
    //    glGenVertexArrays(1, &skyboxVAO);
    //    glGenBuffers(1, &skyboxVBO);
    //    glBindVertexArray(skyboxVAO);
    //    glBindBuffer(GL_ARRAY_BUFFER, skyboxVBO);
    //    glBufferData(GL_ARRAY_BUFFER, sizeof(skyboxVertices), &skyboxVertices, GL_STATIC_DRAW);
    //    glEnableVertexAttribArray(0);
    //    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), (void*)0);
    //    glBindVertexArray(0);

    //    //使用几何着色器
    //    float points[] = {
    //        -0.5f,  0.5f, 1.0f, 0.0f, 0.0f, // 左上
    //        0.5f,  0.5f, 0.0f, 1.0f, 0.0f, // 右上
    //        0.5f, -0.5f, 0.0f, 0.0f, 1.0f, // 右下
    //        -0.5f, -0.5f, 1.0f, 1.0f, 0.0f  // 左下
    //    };

    //    // skybox VAO
    //    unsigned int geomVAO, geomVBO;
    //    glGenVertexArrays(1, &geomVAO);
    //    glGenBuffers(1, &geomVBO);
    //    glBindVertexArray(geomVAO);
    //    glBindBuffer(GL_ARRAY_BUFFER, geomVBO);
    //    glBufferData(GL_ARRAY_BUFFER, sizeof(points), &points, GL_STATIC_DRAW);
    //    glEnableVertexAttribArray(0);
    //    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
    //    glEnableVertexAttribArray(1);
    //    //    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)(2 * sizeof(float)));
    //    //    glBindVertexArray(0);

    //    vector<std::string> faces
    //    {
    //        getFilePath("/resource/skybox/right.jpg").toStdString(),
    //                getFilePath("/resource/skybox/left.jpg").toStdString(),
    //                getFilePath("/resource/skybox/top.jpg").toStdString(),
    //                getFilePath("/resource/skybox/bottom.jpg").toStdString(),
    //                getFilePath("/resource/skybox/front.jpg").toStdString(),
    //                getFilePath("/resource/skybox/back.jpg").toStdString()
    //    };
    //    unsigned int skyTexture = loadCubemap(faces);
    //    unsigned int cubeTexture = loadTexture(getFilePath("/resource/container2.png").toUtf8().data());

    //    // shader configuration
    //    // --------------------
    //    boxShader.use();
    //    boxShader.setInt("texture1", 0);

    //    //    skyShader.use();
    //    //    skyShader.setInt("skybox", 0);


    //渲染循环(Render Loop)
    while(!glfwWindowShouldClose(window))
    {
        //记录时间
        float currentFrame = static_cast<float>(glfwGetTime());
        deltaTime = currentFrame - lastFrame;
        lastFrame = currentFrame;

        //输入
        processInput(window);
        // 渲染指令
        glClearColor(0.1f, 0.1f, 0.1f, 0.1f);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);//清除颜色和深度缓冲区！

        // configure transformation matrices
        glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
        glm::mat4 view = camera.GetViewMatrix();
//        asteroidShader.use();
//        asteroidShader.setMat4("projection", projection);
//        asteroidShader.setMat4("view", view);
        planetShader.use();
        planetShader.setMat4("projection", projection);
        planetShader.setMat4("view", view);

//         draw planet
        glm::mat4 model = glm::mat4(1.0f);
        model = glm::translate(model, glm::vec3(0.0f, -3.0f, 0.0f));
        model = glm::scale(model, glm::vec3(4.0f, 4.0f, 4.0f));
        planetShader.setMat4("model", model);
        planet.Draw(planetShader);

        // draw meteorites
//        asteroidShader.use();
//        asteroidShader.setInt("texture_diffuse1", 0);
//        glActiveTexture(GL_TEXTURE0);
//        glBindTexture(GL_TEXTURE_2D, rock.textures_loaded[0].id); // note: we also made the textures_loaded vector public (instead of private) from the model class.
//        for (unsigned int i = 0; i < rock.meshes.size(); i++)
//        {
//            glBindVertexArray(rock.meshes[i].VAO);
//            glDrawElementsInstanced(GL_TRIANGLES, static_cast<unsigned int>(rock.meshes[i].indices.size()), GL_UNSIGNED_INT, 0, amount);
//            glBindVertexArray(0);
//        }

        //        if(0){
        //            glm::mat4 projection = glm::perspective(glm::radians(camera.Zoom), (float)SCR_WIDTH / (float)SCR_HEIGHT, 0.1f, 100.0f);
        //            glm::mat4 view = camera.GetViewMatrix();
        //            //            glBindBuffer(GL_UNIFORM_BUFFER, uboMatrices);
        //            //            glBufferSubData(GL_UNIFORM_BUFFER, sizeof(glm::mat4), sizeof(glm::mat4), glm::value_ptr(view));
        //            //            glBindBuffer(GL_UNIFORM_BUFFER, 0);

        //            glm::mat4 model = glm::mat4(1.0f);
        //            //            // draw skybox as last
        //            //            glDepthFunc(GL_LEQUAL);  // change depth function so depth test passes when values are equal to depth buffer's content
        //            //            skyShader.use();
        //            //            skyShader.setMat4("view", glm::mat4(glm::mat3(view)));
        //            //            skyShader.setMat4("projection", projection);
        //            //            // skybox cube
        //            //            glBindVertexArray(skyboxVAO);
        //            //            glActiveTexture(GL_TEXTURE0);
        //            //            glBindTexture(GL_TEXTURE_CUBE_MAP, skyTexture);
        //            //            glDrawArrays(GL_TRIANGLES, 0, 36);
        //            //            glBindVertexArray(0);
        //            //            glDepthFunc(GL_LESS); // set depth function back to default

        //            //            model = glm::mat4(1.0f);
        //            //            boxShader.use();
        //            //            boxShader.setMat4("model", model);
        //            //            boxShader.setVec3("cameraPos", camera.Position);
        //            //            boxShader.setMat4("projection", projection);
        //            //            boxShader.setMat4("view", view);
        //            //            // cubes
        //            //            glBindVertexArray(cubeVAO);
        //            //            glActiveTexture(GL_TEXTURE0);
        //            //            glBindTexture(GL_TEXTURE_2D, cubeTexture);
        //            //            glDrawArrays(GL_TRIANGLES, 0, 36);
        //            //            glBindVertexArray(0);


        //            model = glm::mat4(1.0f);
        //            model = glm::translate(model, glm::vec3(1.0,1.0,-15.0));
        //            defaultShader.use();
        //            defaultShader.setMat4("model", model);
        //            defaultShader.setMat4("projection", projection);
        //            defaultShader.setMat4("view", view);
        //            ourModel.Draw(defaultShader);

        //            model = glm::translate(model, glm::vec3(1.0,10.0,0.0));
        //            defaultShader.use();
        //            defaultShader.setMat4("model", model);
        //            defaultShader.setMat4("projection", projection);
        //            defaultShader.setMat4("view", view);
        //            ourModel.Draw(defaultShader);


        //            //            model = glm::translate(model, glm::vec3(1.0,0.0,10.0));
        //            //            modelShader.use();
        //            //            modelShader.setMat4("model", model);
        //            //            modelShader.setMat4("projection", projection);
        //            //            modelShader.setMat4("view", view);
        //            //            ourModel2.Draw(modelShader);

        //        }

        //        //        geomShader.use();
        //        //        glBindVertexArray(geomVAO);
        //        //        glDrawArrays(GL_POINTS, 0, 4);
        //        //        glBindVertexArray(0);


        // 检查并调用事件，交换缓冲
        glfwPollEvents();
        glfwSwapBuffers(window);
    }

    glfwTerminate();
    return 0;

    //        QApplication a(argc, argv);
    //        return a.exec();

}

/**
 * @brief 然而，当用户改变窗口的大小的时候，视口也应该被调整。
 * 我们可以对窗口注册一个回调函数(Callback Function)，
 * 它会在每次窗口大小被调整的时候被调用。这个回调函数的原型如下：
 * 我们还需要注册这个函数，告诉GLFW我们希望每当窗口调整大小的时候调用这个函数：
 * @param window
 * @param width
 * @param height
 */
void framebuffer_size_callback(GLFWwindow* window, int width, int height)
{
    glViewport(0, 0, width, height);
}


/**
 * @brief 我们同样也希望能够在GLFW中实现一些输入控制，这可以通过使用GLFW的几个输入函数来完成。
 * 我们将会使用GLFW的glfwGetKey函数，它需要一个窗口以及一个按键作为输入。
 * 这个函数将会返回这个按键是否正在被按下。
 * 这里我们检查用户是否按下了返回键(Esc)
 * @param window
 */
void processInput(GLFWwindow *window)
{
    if(glfwGetKey(window, GLFW_KEY_ESCAPE) == GLFW_PRESS)
        glfwSetWindowShouldClose(window, true);
    if (glfwGetKey(window, GLFW_KEY_UP) == GLFW_PRESS)
    {
        mixValue += 0.001f; // change this value accordingly (might be too slow or too fast based on system hardware)
        if(mixValue >= 1.0f)
            mixValue = 1.0f;
    }
    if (glfwGetKey(window, GLFW_KEY_DOWN) == GLFW_PRESS)
    {
        mixValue -= 0.001f; // change this value accordingly (might be too slow or too fast based on system hardware)
        if (mixValue <= 0.0f)
            mixValue = 0.0f;
    }
    if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS)
        camera.ProcessKeyboard(FORWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS)
        camera.ProcessKeyboard(BACKWARD, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS)
        camera.ProcessKeyboard(LEFT, deltaTime);
    if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS)
        camera.ProcessKeyboard(RIGHT, deltaTime);}


void transformation(Shader &ourShader)
{
    glm::mat4 trans1 = glm::mat4(1.0f);
    //注意旋转和位移的顺序
    trans1 = glm::translate(trans1, glm::vec3(0.5f, -0.5f, 0.0f));//位移
    trans1 = glm::rotate(trans1, (float)glfwGetTime(), glm::vec3(0.0f, 0.0f, 1.0f));//旋转

    unsigned int transformLoc = glGetUniformLocation(ourShader.ID, "transform");
    glUniformMatrix4fv(transformLoc, 1, GL_FALSE, glm::value_ptr(trans1));
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);

    //第二个箱子 //现在用新的变换替换均匀矩阵，重新绘制它。
    trans1 = glm::mat4(1.0f);
    trans1 = glm::translate(trans1, glm::vec3(-0.5f, 0.5f, 0.0f));//位移
    float scaleAmount = static_cast<float>(sin(glfwGetTime()));
    trans1 = glm::scale(trans1, glm::vec3(scaleAmount, scaleAmount, 0.0f));//缩放
    glUniformMatrix4fv(glGetUniformLocation(ourShader.ID, "transform"), 1, GL_FALSE, &trans1[0][0]);//这次将矩阵值数组的第一个元素作为其内存指针值
    glDrawElements(GL_TRIANGLES, 6, GL_UNSIGNED_INT, 0);
}

void mouse_callback(GLFWwindow *window, double xposIn, double yposIn)
{
    float xpos = static_cast<float>(xposIn);
    float ypos = static_cast<float>(yposIn);

    if (firstMouse)
    {
        lastX = xpos;
        lastY = ypos;
        firstMouse = false;
    }

    float xoffset = xpos - lastX;
    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top

    lastX = xpos;
    lastY = ypos;

    camera.ProcessMouseMovement(xoffset, yoffset);
}

void scroll_callback(GLFWwindow *window, double xoffset, double yoffset)
{
    camera.ProcessMouseScroll(static_cast<float>(yoffset));
}

QString getFilePath(QString path)
{
    QString container2Png = currentDir.replace("/","\\") + path.replace("/","\\");
    return container2Png;
}

unsigned int loadCubemap(std::vector<std::string> faces)
{
    unsigned int textureID;
    glGenTextures(1, &textureID);
    glBindTexture(GL_TEXTURE_CUBE_MAP, textureID);

    int width, height, nrChannels;
    for (unsigned int i = 0; i < faces.size(); i++)
    {
        unsigned char *data = stbi_load(faces[i].c_str(), &width, &height, &nrChannels, 0);
        if (data)
        {
            glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i,
                         0, GL_RGB, width, height, 0, GL_RGB, GL_UNSIGNED_BYTE, data
                         );
            stbi_image_free(data);
        }
        else
        {
            std::cout << "Cubemap texture failed to load at path: " << faces[i] << std::endl;
            stbi_image_free(data);
        }
    }
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
    glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);

    return textureID;
}

unsigned int loadTexture(const char *path)
{
    unsigned int textureID;
    glGenTextures(1, &textureID);

    int width, height, nrComponents;
    unsigned char *data = stbi_load(path, &width, &height, &nrComponents, 0);
    if (data)
    {
        GLenum format;
        if (nrComponents == 1)
            format = GL_RED;
        else if (nrComponents == 3)
            format = GL_RGB;
        else if (nrComponents == 4)
            format = GL_RGBA;

        glBindTexture(GL_TEXTURE_2D, textureID);
        glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
        glGenerateMipmap(GL_TEXTURE_2D);

        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

        stbi_image_free(data);
    }
    else
    {
        std::cout << "Texture failed to load at path: " << path << std::endl;
        stbi_image_free(data);
    }

    return textureID;
}
