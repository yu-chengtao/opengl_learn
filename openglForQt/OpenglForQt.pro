QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

CONFIG += c++11

# You can make your code fail to compile if it uses deprecated APIs.
# In order to do so, uncomment the following line.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0


SOURCES += \
    main.cpp \
    openglTool/mesh.cpp \
    openglTool/model.cpp \
    openglTool/shader.cpp \
    openglTool/stb_image.cpp

HEADERS += \
    main.h \
    openglTool/camera.h \
    openglTool/mesh.h \
    openglTool/model.h \
    openglTool/shader.h \
    openglTool/stb_image.h

DISTFILES += \
    resource/awesomeface.png \
    resource/blending_transparent_window.png \
    resource/container2.png \
    resource/container2_specular.png \
    resource/grass.png \
    resource/matrix.jpg \
    resource/wall.jpg \
    shaders/asteroids.frag \
    shaders/asteroids.vert \
    shaders/box.frag \
    shaders/box.vert \
    shaders/default.frag \
    shaders/default.vert \
    shaders/geometry.frag \
    shaders/geometry.geom \
    shaders/geometry.vert \
    shaders/instance.frag \
    shaders/instance.vert \
    shaders/lamp.frag \
    shaders/lightingSharder.frag \
    shaders/lightingSharder.vert \
    shaders/model.frag \
    shaders/model.geom \
    shaders/model.vert \
    shaders/planet.frag \
    shaders/planet.vert \
    shaders/sky.frag \
    shaders/sky.vert


# Default rules for deployment.
qnx: target.path = /tmp/$${TARGET}/bin
else: unix:!android: target.path = /opt/$${TARGET}/bin
!isEmpty(target.path): INSTALLS += target


#Opengl - Glad
include($$PWD/3rdparty/glad/glad.pri)

#   Opengl Glfw
LIBS += -lopengl32 -luser32

win32: LIBS += -L$$PWD/3rdparty/glfw/lib-vc2017/ -lglfw3dll
win32: LIBS += -L$$PWD/3rdparty/assimp-5.1.1/lib-msvc2017/ -lassimp-vc142-mtd

INCLUDEPATH += $$PWD/3rdparty/glfw/include
DEPENDPATH += $$PWD/3rdparty/glfw/include
INCLUDEPATH += $$PWD/3rdparty/assimp-5.1.1/include
DEPENDPATH += $$PWD/3rdparty/assimp-5.1.1/include

INCLUDEPATH += $$PWD/3rdparty/glm-0.9.9.7/glm
DEPENDPATH += $$PWD/3rdparty/glm-0.9.9.7/glm
INCLUDEPATH += $$PWD/3rdparty/glm-0.9.9.7/glm/gtc
DEPENDPATH += $$PWD/3rdparty/glm-0.9.9.7/glm/gtc

#pro中的变量可以通过宏定义的方式在代码中调用
DEFINES += MY_CUSTOM_PATH=\\\"$$OUT_PWD/resc\\\"

# 配置file_copies
  CONFIG += file_copies
  # 创建examples变量并配置
  # 配置需要复制的文件或目录(支持通配符)
  drivers.files += $$PWD/resource
  drivers.files += $$PWD/shaders
  # 配置需要复制的目标目录, $$OUT_PWD为QMake内置变量，含义为程序输出目录
  drivers.path = $$OUT_PWD/resc

  # 配置COPIES
  COPIES += drivers


