#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include "mesh.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>

/**
 * @brief 模型类
 */
class Model
{
public:
    std::vector<Texture> textures_loaded;
public:
    /*  函数   */
    Model(char *path)
    {
        loadModel(path);
    }
    void Draw(Shader shader);
public:
    /*  模型数据  */
    std::vector<Mesh> meshes;
    std::string directory;
    /*  函数   */
    void loadModel(std::string path);
    void processNode(aiNode *node, const aiScene *scene);
    Mesh processMesh(aiMesh *mesh, const aiScene *scene);
    std::vector<Texture> loadMaterialTextures
    (aiMaterial *mat, aiTextureType type, std::string typeName);
};

#endif // MODEL_H
