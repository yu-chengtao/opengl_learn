#pragma once

#include <iostream>
using namespace std;
#include <glad/glad.h>
#include <GLFW/glfw3.h>
#include "util/stb_image.h"

#include "util/shader.h"
#include "util/camera.h"
#include "util/vertex.h"
#include "util/model.h"

#include "util/ResourceManager.h"


using std::stringstream;

namespace learn{

#define print(x) cout << x << endl;
#define len(x) sizeof(x)/sizeof(x[0])
#define type(x) typeid(x).name()
#define printV3(x) cout << "(" << x[0] << "," << x[1] << ","<< x[2] << ")"
#define printV2(x) cout << "(" << x[0] << "," << x[1] << ")"

void framebuffer_size_callback(GLFWwindow* window, int width, int height);
void mouse_callback(GLFWwindow* window, double xpos, double ypos);
void scroll_callback(GLFWwindow* window, double xoffset, double yoffset);
void processInput(GLFWwindow* window);
unsigned int loadTexture(const char* path);
unsigned int loadCubemap(vector<std::string> faces);

#pragma region ����
// settings
static const unsigned int SCR_WIDTH = 800;
static const unsigned int SCR_HEIGHT = 600;
static Camera camera(glm::vec3(0.0f, 0.0f, 3.0f));
static float lastX = SCR_WIDTH / 2.0f;
static float lastY = SCR_HEIGHT / 2.0f;
static bool firstMouse = true;

// timing
static float deltaTime = 0.0f;	// time between current frame and last frame
static float lastFrame = 0.0f;
#pragma endregion

int learnRendering(); 

}