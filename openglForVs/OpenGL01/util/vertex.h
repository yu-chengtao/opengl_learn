#pragma once


#include <glad/glad.h>
#include <GLFW/glfw3.h>

class MyVertex {
public:
	MyVertex(float vertices[], int vertSize,
		unsigned int indices[], int indiSize,
		unsigned int &VBO, 
		unsigned int &VAO, 
		unsigned int &EBO) {
		glGenVertexArrays(1, &VAO);
		glGenBuffers(1, &VBO);
		glGenBuffers(1, &EBO);
		glBindVertexArray(VAO);

		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, vertSize, vertices, GL_STATIC_DRAW);

		if (indiSize == 0 || indices == NULL)
			return;
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indiSize, indices, GL_STATIC_DRAW);

	}


	void setProperty(int pId, int pSize, int vSize, int pOffset) {
		glVertexAttribPointer(pId, pSize, GL_FLOAT, GL_FALSE, vSize * sizeof(float), (void*)(pOffset * sizeof(float)));
		glEnableVertexAttribArray(pId);
	}







};