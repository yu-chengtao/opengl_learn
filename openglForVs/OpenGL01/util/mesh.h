#ifndef MESH_H
#define MESH_H

#include <iostream>
#include <vector>
#include "shader.h"

#include "Resource.h"

#define MAX_BONE_INFLUENCE 4

using std::vector;
using std::string;

struct Vertex
{
	glm::vec3 Position;
	glm::vec3 Normal;
	glm::vec2 TexCoords;
	glm::vec3 Tangent;
	glm::vec3 Bitangent;
};

struct Texture
{
	GLuint id;
	std::string type;
	aiString Path;
};

class Mesh
{
public:
	vector<Vertex> vertices;
	vector<GLuint> indices;
	vector<Texture> textures;

	Mesh(vector<Vertex> vertices, vector<GLuint> indices, vector<Texture> textures);

	~Mesh();

	void Draw(Shader& shader);

private:

	GLuint VBO, VAO, EBO;

	void SetUpMesh();
};

#endif // MESH_H
