#ifndef MODEL_H
#define MODEL_H

#include <vector>
#include "mesh.h"
#include <assimp/Importer.hpp>
#include <assimp/scene.h>
#include <assimp/postprocess.h>
#include <map>

using std::vector;
using std::string;
using std::map;

class Model
{
public:
    /*  函数   */
    Model(const char* path)
    {
        LoadModel(path);
    }
    void Draw(Shader& shader);
private:
    /*  模型数据  */
    vector<Mesh> meshes;
    string directory;
    map<string, Texture> textures_loaded;

    void LoadModel(string path);
    void ProcessNode(aiNode* node, const aiScene* scene);
    Mesh ProcessMesh(aiMesh* mesh, const aiScene* scene);
    vector<Texture> LoadMaterialTextures(aiMaterial* mat, aiTextureType type,
        string typeName, const aiScene* scene);
};

namespace StringUtil
{
    template<typename ... Args>
    std::string Format(const std::string& format, Args ... args) //暂时不用C++20
    {
        size_t size = snprintf(nullptr, 0, format.c_str(), args ...) + 1; // Extra space for '\0'
        if (size <= 0) { throw std::runtime_error("Error during formatting."); }
        std::unique_ptr<char[]> buf(new char[size]);
        snprintf(buf.get(), size, format.c_str(), args ...);
        return std::string(buf.get(), buf.get() + size - 1);
    }
}


#endif // MODEL_H
