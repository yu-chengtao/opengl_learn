/*******************************************************************
** This code is part of Breakout.
**
** Breakout is free software: you can redistribute it and/or modify
** it under the terms of the CC BY 4.0 license as published by
** Creative Commons, either version 4 of the License, or (at your
** option) any later version.
******************************************************************/
#include <glad/glad.h>
#include <GLFW/glfw3.h>

#include "util/ResourceManager.h"

#include <iostream>

#include "learn.h"


int main(int argc, char* argv[])
{
	return learn::learnRendering();

}
